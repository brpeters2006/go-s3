FROM alpine:latest

COPY go-s3 .

RUN  adduser -D -u 8877 go

USER go

CMD ["/bin/sh", "-c", "./go-s3"]

